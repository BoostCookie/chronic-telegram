#!/usr/bin/env python3

import selectors
import sys
import socket
import subprocess
import os
import asyncio
import fcntl
from typing import List
from datetime import datetime, timezone

command_name = ""
message_header = ""
started_running = datetime.now(timezone.utc).astimezone()

async def telegram_message(message):
    import telegram

    config_path = "/etc/chronic-telegram/telegram-bot.conf"
    if not os.path.isfile(os.path.realpath(config_path)):
        raise Exception("Config file {} does not exist!".format(config_path))

    token = ""
    chat_ids = []
    with open(config_path, "r") as f:
        for i, line in enumerate(f):
            line = line.strip()
            if not line:
                continue
            elif line.startswith("token="):
                token = line[len("token="):]
            elif line.startswith("chat_ids="):
                chat_ids += [int(chat_id_str) for chat_id_str in line[len("chat_ids="):].split(",")]
            else:
                print("chronic-telegram: encountered unkown argument in line {}!".format(i), file=sys.stderr)

    if token == "":
        raise Exception("No token or empty token in config file!")

    global message_header
    if (datetime.now(timezone.utc).astimezone() - started_running).total_seconds() > 200:
        message_header += "Started running at {}\n".format(started_running.isoformat(timespec="seconds"))
    full_message = message_header + message

    bot = telegram.Bot(token=token)
    message_too_long = False
    retry = True
    formatting = True
    while retry:
        retry = False
        for chat_id in chat_ids:
            try:
                await bot.send_message(chat_id, full_message, parse_mode="HTML" if formatting else None)
            except telegram.error.BadRequest as e:
                if e.message == "Message is too long":
                    message_too_long = True
                    break
                elif "parse entit" in e.message:
                    # try without formatting
                    print("{}\nTrying without formatting".format(e))
                    message_header += "{}\nTrying without formatting:\n".format(e)
                    full_message = message_header + message
                    formatting = False
                    retry = True
                    break
                else:
                    raise e

    if message_too_long:
        tempfilename = "{}-{}.txt".format(socket.gethostname(), command_name.replace("/", "_"))
        tempfilepath = os.path.join("/tmp", tempfilename)
        with open(tempfilepath, "w") as f:
            f.write(full_message)
        telegram_message = await bot.send_document(chat_ids[0], open(tempfilepath, "rb"), tempfilename)
        for chat_id in chat_ids[1:]:
            assert telegram_message.document
            await bot.send_document(chat_id, telegram_message.document.file_id, tempfilename)
        os.remove(tempfilepath)

def shellify(command_part: str) -> str:
    chars_to_be_escaped: List[str] = ["`", "~", "!", "#", "$", "&", "*", "(", ")", "\t", "{", "[", "|", ";", "'", '"', "\n", "<", ">", "?", " "]
    for c in chars_to_be_escaped:
        if c in command_part:
            command_part = "'" + command_part + "'"
            break
    command_part.replace("\\", r"\\")
    return command_part

class LineReader:
    def __init__(self):
        self.buf = ""

    def append(self, s: str):
        self.buf += s

    def read(self):
        lines = self.buf.split("\n")
        for (i, line) in enumerate(lines):
            if i == len(lines) - 1:
                self.buf = line
                return None
            yield line


def convert_to_non_blocking_reader(reader):
    fd = reader.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)


async def main():
    if "-h" in sys.argv[1:] or "--help" in sys.argv[1:] or len(sys.argv[1:]) < 1:
        print("""Usage: chronic-telegram [-e] [-v] [--] [PROGRAM_ARG1 ...]

Options:
    -e: Trigger on stderr output, even when the return code is zero.
    -v: Be more verbose to distinguish which output was stdout and which was stderr.""")
        sys.exit(0)

    options = {
            "v": False, #  verbose output
            "e": False, #  stderr triggering
            }
    command_start_i = 0
    for i, arg in enumerate(sys.argv[1:]):
        if not arg.startswith("-"):
            command_start_i = i + 1
            break
        elif arg == "--":
            command_start_i = i + 2
            break
        for char in arg[1:]:
            if char not in options.keys():
                raise Exception("Unknown option -{}".format(char))
            options[char] = True

    global command_name
    command_name = " ".join([shellify(c) for c in sys.argv[command_start_i:]])
    global message_header
    message_header = "{}: {}:\n".format(socket.gethostname(), command_name)

    process = subprocess.Popen(sys.argv[command_start_i:], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    assert process.stdout is not None
    assert process.stderr is not None

    convert_to_non_blocking_reader(process.stdout)
    convert_to_non_blocking_reader(process.stderr)

    sel = selectors.DefaultSelector()

    sel.register(process.stdout, selectors.EVENT_READ, "STDOUT")
    sel.register(process.stderr, selectors.EVENT_READ, "STDERR")

    message = ""

    stderr_is_not_empty = False

    is_eof = {
        "STDOUT": False,
        "STDERR": False,
    }
    out_files = {
        "STDOUT": sys.stdout,
        "STDERR": sys.stderr
    }

    line_readers = {
        "STDOUT": LineReader(),
        "STDERR": LineReader()
    }

    while not all(is_eof.values()):
        for key, _ in sel.select():
            text = key.fileobj.read() # type: ignore
            if text:
                if key.data == "STDERR":
                    stderr_is_not_empty = True

                line_readers[key.data].append(text)
                print(text, end="", file=out_files[key.data])
                # The output of our process is line-buffered, as expected on UNIX systems.
                # Uncomment this if you want incomplete lines to show up immediately,
                # out_files[key.data].flush()
            else:
                is_eof[key.data] = True


        for name, reader in line_readers.items():
            for line in reader.read():
                if options["v"]:
                    message += name + ": "
                message += line + "\n"


    for name, reader in line_readers.items():
        if reader.buf:
            # The program did not terminate its output with a '\n'. (Grrr)
            # Like zsh, we add it but inform the user that we changed their output by adding a %
            if options["v"]:
                message += name + ": "
            message += reader.buf + "<b>%</b>\n"

    process.wait()
    if (process.returncode != 0) or (options["e"] and stderr_is_not_empty):
        if options["v"]:
            message += "RETVAL: {}".format(process.returncode)
        message = message.strip()
        await telegram_message(message)

    sys.exit(process.returncode)

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except Exception as e:
        ct_error = "Error in {}:\n".format(sys.argv[0])
        message_header += ct_error
        print(ct_error, end="")
        print(e)
        asyncio.run(telegram_message("{}".format(e)))
        raise e
