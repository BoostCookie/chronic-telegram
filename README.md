# chronic-telegram
Sends the output of a failed command via telegram.

The token of the bot and the chat-ids of the lucky receivers are supposed to be in `/etc/chronic-telegram/telegram-bot.conf`:
```
token=1234567890:ABCdef123GHIjklMNO_PQR
chat_ids=1337,42,420,69
```

Test your setup via
```
./chronic-telegram.py -e sh -c "echo hello error >> /dev/stderr"
```
